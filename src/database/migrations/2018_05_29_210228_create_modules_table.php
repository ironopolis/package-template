<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_id')->unsigned();
            $table->integer('user_id')->unsigned()->default(0);
            $table->integer('level')->unsigned();
            $table->string('title');
            $table->text('description');
            $table->decimal('amount', 8, 2);
            $table->string('slug', 100);
            $table->string('template', 100);
            $table->text('data');
            $table->enum('status', ['draft', 'live', 'archived'])->default('draft');
            $table->date('start');
            $table->date('end');
            $table->softDeletes();
            $table->timestamps();

            // indexes
            $table->index('user_id');
            $table->unique(['module_id', 'slug']);
            $table->index('status');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modules');
    }
}
