<?php

namespace Ironopolis\Course;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    public function path() {
        return '/courses/' . $this->id;
    }

    public function module() {
        return $this->hasMany(Module::class);
    }
}
