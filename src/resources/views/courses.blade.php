@extends('homepage::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">Subjects</div>
                    @if (!empty($courses))
                        <div class="card-body">
                            @foreach($courses as $course)
                                <article>
                                    <a href="{{ $course->path() }}">
                                        <h4>{{ $course->title }}</h4>
                                    </a>
                                </article>
                            @endforeach
                        </div>
                    @endif
            </div>
        </div>
    </div>
</div>
@endsection