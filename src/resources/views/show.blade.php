@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">{{ $course->title or '' }}</div>

                <div class="card-body">
                  {{ $course->description or '' }}
                </div>

                    @foreach($course->module as $module)
                        <article>
                                <h4>{{ $module->slug }}</h4>
                        </article>
                    @endforeach

            </div>
        </div>
    </div>
</div>
@endsection