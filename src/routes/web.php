<?php

Route::get('courses', 'Ironopolis\Course\Http\Controllers\CourseController@index')->middleware('web');

Route::get('courses/{course}', 'Ironopolis\Course\Http\Controllers\CourseController@show')->middleware('web');
